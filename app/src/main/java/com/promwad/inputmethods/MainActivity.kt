package com.promwad.inputmethods

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.inputmethod.CompletionInfo
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity

class MainActivity : AppCompatActivity() {

    private lateinit var mEditText: EditText

    private val inputMethodManager: InputMethodManager
        get() = getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        mEditText = findViewById(R.id.ims_editor)
        mEditText.addTextChangedListener(Watcher())
    }

    inner class Watcher : TextWatcher {
        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}

        override fun afterTextChanged(s: Editable?) {
            inputMethodManager
                .displayCompletions(mEditText, arrayOf(CompletionInfo(0L, 0, s)))
        }

    }

}
